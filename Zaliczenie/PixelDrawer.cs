﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zaliczenie
{
    class PixelDrawer
    {
        int width, height, posX, posY;
        public Color color;
        public Texture2D texture;

        public PixelDrawer(int w, int h, int x, int y)
        {
            width = w;
            height = h;
            posX = x;
            posY = y;
            color = Color.White;
                
        }
        
        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(texture, new Rectangle(posX, posY, width, height), color);
        }
    }
}
