﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Zaliczenie.Screens;

namespace Zaliczenie
{

    public class Game1 : Game
    {
        public enum AppState { Splashscreen, MainMenu }
        public static AppState appState = AppState.MainMenu;
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        PixelDrawer pixelDrawer;
        int WINDOW_WIDTH = 1080, WINDOW_HEIGHT = 720;
        public static SpriteFont spriteFont;

        Screen currentScreen;

        public Game1()
        {
            this.IsMouseVisible = true;
            graphics = new GraphicsDeviceManager(this)
            {
                PreferredBackBufferWidth = WINDOW_WIDTH,
                PreferredBackBufferHeight = WINDOW_HEIGHT
            };
            graphics.ApplyChanges();

            Content.RootDirectory = "Content";
        }

        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            pixelDrawer = new PixelDrawer(10, 10, 10, 10);
            currentScreen = new Screen(this);
            base.Initialize();
        }


        protected override void LoadContent()
        {

            spriteBatch = new SpriteBatch(GraphicsDevice);
            pixelDrawer.texture = Content.Load<Texture2D>("pixel");
            spriteFont = Content.Load<SpriteFont>("MainFont");
            // TODO: use this.Content to load your game content here
        }


        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            switch (appState)
            {
                case AppState.Splashscreen:
                    currentScreen = new Screen(this);
                    break;
                case AppState.MainMenu:
                    currentScreen = new MainMenu();
                    break;
                default:
                    break;
            }
            // TODO: Add your update logic here
            currentScreen.Update(gameTime);
            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Wheat);
            // TODO: Add your drawing code here
            spriteBatch.Begin();
            currentScreen.Draw(spriteBatch);
            //pixelDrawer.Draw(spriteBatch);

            spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}
