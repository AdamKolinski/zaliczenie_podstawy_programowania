﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zaliczenie.Screens
{
    class MainMenu : Screen
    {
        public Texture2D tex;
        bool draw;
        List<String> menuItems;

        public MainMenu() : base(g)
        {
            title = "Main menu";
            g.Window.Title = title;
        }

        public override void LoadContent()
        {
            tex = g.Content.Load<Texture2D>("ball");
        }
        public override void Update(GameTime gameTime) {
            mouseState = Mouse.GetState();

            if (mouseState.LeftButton == ButtonState.Pressed)
            {
                draw = true;
            }
            else
            {
                draw = false;
            }
            if (mouseState.RightButton == ButtonState.Pressed)
            {
                Game1.appState = Game1.AppState.Splashscreen;
            }

        }
        public override void Draw(SpriteBatch spriteBatch)
        {
            if (draw)
            {
                spriteBatch.Draw(tex, new Rectangle(mouseState.X, mouseState.Y, 50, 50), Color.White);
            }
            spriteBatch.DrawString(Game1.spriteFont, title, new Vector2(0, 0), Color.Black);
        }
    }
}
