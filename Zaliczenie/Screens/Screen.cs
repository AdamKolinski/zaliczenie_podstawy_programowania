﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zaliczenie
{
    class Screen
    {
        public string title = "Splashscreen";
        public static Game1 g;
        public MouseState mouseState;

        public Screen(Game1 game)
        {
            g = game;
            g.Window.Title = title;
            LoadContent();
        }

        public virtual void LoadContent() {
            
        }
        public virtual void Update(GameTime gameTime) { }
        public virtual void Draw(SpriteBatch spriteBatch) {
            spriteBatch.DrawString(Game1.spriteFont, title, new Vector2(0, 0), Color.White);
        }
    }
}
